// Timer
#include <SimpleTimer.h>
SimpleTimer timer;
int timerSpeed = -1;
int timerSendMessage = -1;
int led = 13;

String myAck;
String commandAck = "";
byte ackNumber = 9;
byte retry = 0;
int timerWaitAck = -1;
int timerAck = -1;
#define maxRetry 5

// Serial incomming
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

// nrf24_server.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messageing server
// with the RH_NRF24 class. RH_NRF24 class does not provide for addressing or
// reliability, so you should only use RH_NRF24  if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example nrf24_client
// Tested on Uno with Sparkfun NRF25L01 module
// Tested on Anarduino Mini (http://www.anarduino.com/mini/) with RFM73 module
// Tested on Arduino Mega with Sparkfun WRL-00691 NRF25L01 module

#include <SPI.h>
#include <RH_NRF24.h>

// Singleton instance of the radio driver
RH_NRF24 nrf24;
// RH_NRF24 nrf24(8, 7); // use this to be electrically compatible with Mirf
// RH_NRF24 nrf24(8, 10);// For Leonardo, need explicit SS pin
// RH_NRF24 nrf24(8, 7); // For RFM73 on Anarduino Mini

//Test
int speed = 0;


void setup() 
{
  Serial.begin(9600);
  while (!Serial) 
    ; // wait for serial port to connect. Needed for Leonardo only
  if (!nrf24.init())
    Serial.println("Init network failed");
  // Defaults after init are 2.402 GHz (channel 2), 2Mbps, 0dBm
  if (!nrf24.setChannel(1))
    Serial.println("setChannel failed");
  if (!nrf24.setRF(RH_NRF24::DataRate2Mbps, RH_NRF24::TransmitPower0dBm))
    Serial.println("setRF failed");        
 //timerSpeed = timer.setInterval(2000, stepperSpeed);  //Send a message via RadioHead
  Serial.println("Bridge starting...");   

  // reserve 200 bytes for the inputString:
  inputString.reserve(200);  
}

void loop()
{
  
  if (nrf24.available())
  {
    // Should be a message for us now   
    uint8_t buf[RH_NRF24_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    if (nrf24.recv(buf, &len))
    {
      
//      NRF24::printBuffer("request: ", buf, len);
      
      String inputString = (char*)buf;  
      // Test if we need ack
      if (inputString.charAt(0) == 'A') {
        // Send Ack
        myAck = inputString.substring(0,3);
        myAck.setCharAt(0, 'R');
        timerAck = timer.setTimeout(100, ackMessage);
        // remove Ack
        inputString = inputString.substring(myAck.length() + 1);
      }      
      if (inputString.charAt(0) == 'R') {
        if (commandAck.substring(1,3).toInt() == inputString.substring(1,3).toInt()) {
          commandAck = "";
          retry = 0;
        }
      } else {
        Serial.println(inputString);
      }
    }

  }
  // Send the string from Serial to NRF24
  if (stringComplete) {
    sendMessage(inputString);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  timer.run();
}

void sendMessage(String message) {
// Add Ack number between 10-99
  if (message.charAt(0) != 'A' && message.charAt(0) != 'R' && message.charAt(0) != '0') {   
    String ackString;
    ackString = "A";
    ackString.concat(ackNumber);
    ackString.concat(" ");
    ackString.concat(message);
    message = ackString;  
    commandAck = message;
    // Launch Timer
    timerWaitAck = timer.setTimeout(460, waitAck);
    ackNumber++;    
    if (ackNumber == 100) {
      ackNumber = 9;
    }
  }
  Serial.print("Sending : ");
  Serial.println(message);
  uint8_t data[RH_NRF24_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(data);  
  message.getBytes(data, len);
  nrf24.send(data, sizeof(data));
  nrf24.waitPacketSent();
}

void waitAck() {
  if (commandAck.length() > 0 && retry < maxRetry) {
    sendMessage(commandAck);
    timerWaitAck = timer.setTimeout(460, waitAck);
    retry++;
  } else {
    retry = 0;
  }
  
}

void ackMessage() {
  sendMessage(myAck);  
}

void stepperSpeed() {
  String command;
  if (speed == 0) {
    command = "1 STEP 1500";
    speed = 1500;
  } else {
    command = "1 STEP 0";
    speed = 0;
  }
  sendMessage(command); 
}


/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {

  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {      
      stringComplete = true;
    }
  }
}
