/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#define VERSION "1.0.1"

// Configuration
#define CORRECT_LEVEL0 0 
#define CORRECT_LEVEL1 40 // Correction when you're not on the good state (in percent)
#define CORRECT_LEVEL2 70 // Correction when you're not on the good state (30%)
#define CORRECT_LEVEL3 99 // if correction > 100 a wheel turn rear

#define LIGHTS 1 // Light kit is installed
#define PIN_LIGHT_BACK 14 // A0
#define PIN_LIGHT_LEFT 15 // A1
#define PIN_LIGHT_RIGHT 16 // A2
#define PIN_LIGHT_FRONT 17 // A3

#define BLINK_INTERVAL 500

#define HF_NUMLBER 2 // headlight flashing number
#define HF_INTERVAL 200

//General
//int LED_PIN = 13; //Already used by NRF
int id;
byte mode; //1 => Normal, 0 => Simple
// Change mode of the car 2 MODE 0

byte speedSimple; //Speed of the car in simple mode
byte levelSpeeds[7];
byte levelSpeed = 0; // Store the level speed

// Timer
#include <SimpleTimer.h>
SimpleTimer timer;

// reset
#include <avr/wdt.h>

// DC motor
#define motorLDirectionPin  2     // Direction for left motor
#define motorLPin  3     // PWM for left motor
#define motorRDirectionPin  4     // Direction for right motor
#define motorRPin  5     // PWM for right motor

// Define forward direction for each motor
#define motorLDirection  true     // Default direction
#define motorRDirection  false     //  true = HIGH - false = LOW

int timerSetDCSpeed = -1;

// Radiohead
#include <SPI.h>
#include <RH_NRF24.h>

// RFID
#include <MFRC522.h>
#define SS_PIN 7
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
MFRC522::MIFARE_Key key;//create a MIFARE_Key struct named 'key', which will hold the card information

int timerRFID = -1;


// Singleton instance of the radio driver
RH_NRF24 nrf24;
//Test sending message
int timerSendMessage = -1;
int timerGetMessage = -1;
int timerAlive = -1;
int timerBlink = -1; // Lights : Blink lights
int timerHF = -1; // Lights : Headlight flashing
int timerStop = -1; // Lights : brake


//TCRT5000L
int timerReadSensor = -1;
byte sensorValue = 0;


void setup() {

  Serial.begin(9600);
  Serial.print("Version : ");    
  Serial.println(VERSION); 
  
  id = eepromReadId();
  mode = eepromReadMode();
  eepromReadLevelsSpeeds();
  //Dc motors
  pinMode(motorLDirectionPin, OUTPUT);
  pinMode(motorLPin, OUTPUT);  
  pinMode(motorRDirectionPin, OUTPUT);  
  pinMode(motorRPin, OUTPUT);    
  
  //Init Timers
//  timerSendMessage = timer.setInterval(2000, sendMessageTest);  //Send a message via RadioHead
  timerReadSensor = timer.setInterval(10, getState);  //Read TCRT5000L Sensors
  timerSetDCSpeed = timer.setInterval(16, setDCSpeed);  //Set DC speed if changed
  timerAlive = timer.setInterval(10000, alive);  //Alive every 10s
  
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;//keyByte is defined in the "MIFARE_Key" 'struct' definition in the .h file of the library
  }
  if (mode == 0) {
    speedSimple = eepromReadSpeedSimple();
    if (speedSimple > 100) speedSimple = 100;
    Serial.print("speedSimple : ");    
    Serial.println(speedSimple); 
    setCarSpeed(String(String(id) + " SP " + String(speedSimple)));
    return;
  }

  if (LIGHTS) {
    pinMode(PIN_LIGHT_BACK, OUTPUT); 
    pinMode(PIN_LIGHT_LEFT, OUTPUT); 
    pinMode(PIN_LIGHT_RIGHT, OUTPUT); 
    pinMode(PIN_LIGHT_FRONT, OUTPUT);     
  }

  // Init RFID
  SPI.begin();            // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card  
  mfrc522.PCD_SetAntennaGain(mfrc522.RxGain_max);  
  //Init RadioHead
  if (!nrf24.init())
    Serial.println("Network init failed");
  // Defaults after init are 2.402 GHz (channel 2), 2Mbps, 0dBm
  if (!nrf24.setChannel(1))
    Serial.println("setChannel failed");
  if (!nrf24.setRF(RH_NRF24::DataRate2Mbps, RH_NRF24::TransmitPower0dBm))
    Serial.println("setRF failed"); 
  Serial.println("starting"); 
  timerGetMessage = timer.setInterval(50, getMessage);  //get a message via RadioHead
  timerRFID = timer.setInterval(52, scanRFID);  //read RFID  
  // Say you are here
  alive();
  alive(); // Twice to avoid missing caracter
}//--(end setup )---

void loop() {
  timer.run();
}


/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}

/**
 * Reset arduino
 */
void softwareReboot()
{
  asm volatile ("jmp 0");
}
