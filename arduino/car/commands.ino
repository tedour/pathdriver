/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */


/**
* Dispatch commands from Serial
*/
  uint8_t buf[RH_NRF24_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);


/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */

 
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
String myAck;
int timerAck = -1;

String commandAck = "";
byte ackNumber = 9;
byte retry = 0;
int timerWaitAck = -1;
#define maxRetry 5


void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
      dispatchCommand(inputString);
      
      inputString = "";
    }
  }
}

void sendMessage(String message) {
  if (mode == 0) return; // Simple mode
  message = message + "\n";
  // Add Ack number between 1-99
  if (message.charAt(0) != 'A') {   
    String ackString;
    ackString = "A";
    ackString.concat(ackNumber);
    ackString.concat(" ");
    ackString.concat(message);
    message = ackString;  
    commandAck = message;
    // Launch Timer
    timerWaitAck = timer.setTimeout(230, waitAck);
    ackNumber++;    
    if (ackNumber == 100) {
      ackNumber = 9;
    }
  }  
  Serial.print("Send : ");  
  Serial.println(message);
  timer.restartTimer(timerAlive);
  if (mode == 0) return;
  uint8_t data[RH_NRF24_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(data);  
  message.getBytes(data, len);
  nrf24.send(data, sizeof(data));
  nrf24.waitPacketSent();
}

void waitAck() {
  if (commandAck.length() > 0 && retry < maxRetry) {
    sendMessage(commandAck);
    if (true) {
     String debug;
     debug.concat("RT:");   
     debug.concat(retry);
     Serial.println(debug);
    }
    timerWaitAck = timer.setTimeout(460, waitAck);
    retry++;
  } else {
    retry = 0;
  }
  
}

/*
  Get a command from NRF24
*/
void getMessage() {
  if (mode == 0) return;
  // Now wait for a reply
  // Should be a reply message for us now
  if (nrf24.recv(buf, &len))
  {
    Serial.print("Get : ");
    Serial.println((char*)buf);
    inputString = (char*)buf;
    // Test if we need ack
    if (inputString.charAt(0) == 'A' && getParameter(inputString, 1).toInt() == id) {
      // Send Ack
      myAck = getParameter(inputString, 0);
      myAck.setCharAt(0, 'R');
      timerAck = timer.setTimeout(100, ackMessage);
      // remove Ack
      inputString = inputString.substring(myAck.length() + 1);
    }
    if (inputString.charAt(0) == 'R') {
      if (commandAck.substring(1,3).toInt() == inputString.substring(1,3).toInt()) {
        commandAck = "";
        retry = 0;
      }
    } else {
      dispatchCommand(inputString);
    }
    inputString = "";          
  }
}

void ackMessage() {
  sendMessage(myAck);  
}

/**
 * Get a command and launch the correct void
 * String command = "M100 Z -10 A";
 * getParameter(command, 1); return Z 
 **/
void dispatchCommand(String command) { 
  String idClient = getParameter(command, 0);
  if (true) Serial.println(command);
  // 0 is for all clients, 
  // 1022 is to change the ID : 1022 ID 5 => Set the id to 5
  if (idClient.toInt() != 0 && idClient.toInt() != 1022 && idClient.toInt() != id) { 
    return;
  }
  String mainCommand = getParameter(command, 1);    
  if (mainCommand == "SP") setCarSpeed(command); // Change speed of the car : 2 SP 21
  if (mainCommand == "SPL") setCarSpeedLevel(command); // Change speed level of the car : 2 SPL 2
  if (mainCommand == "TURN") turn(command); // Try to turn next turn 
  if (mainCommand == "CL") changeLine(command); // Change line Right or Left (R - L) : 2 CL R
  if (mainCommand == "ID") eepromWriteId(command); // Store ID of the car in eeprom : 0 ID 3
  if (mainCommand == "MODE") eepromWriteMode(command); // Change mode of the car 1 => Normal, 0 => Simple : 2 MODE 0
  if (mainCommand == "SPS") eepromWriteSpeedSimple(command); // Change speed of the car for simple mode : 2 SPS 30
  if (mainCommand == "WLS") eepromWriteLevelsSpeeds(command); // Change the 7 levels speed : 0 WLS 0 23 28 33 38 43 48
  // Or 0 WLS 0 23 30 37 45 54 63
  if (mainCommand == "RID") RFIDprepareUID(command); // Change UID of RFID Tag and options : 0 RID 2 2
  if (mainCommand == "TC") turnCross(command); // Set turn for the next cross : 2 TC 2 R
  if (mainCommand == "TM") testMotors(command); // Test motors
  if (mainCommand == "RBT") softwareReboot(); // Reboot
  if (LIGHTS) {
    /*
      Light command : 2 LI F O (Light Front ON) 
      Second argument : B(back), L(left), R(right), F(front)
      Third argument : O(on), F(off), B(blink)
    */
    if (mainCommand == "LI") lights(command); // Light command : LI F O (Light Front ON) 
  }
}



/***
 * return the position of the space rank
 * String command = "M100 10";
 * byte result = indexOfRank(command, 1);
 * return 4
 * if not found return 0
 **/

byte indexOfRank (String data, byte rank) {
  byte i = 0;
  byte index = 0;
  while (index != 255 && i < rank) {
  	index = data.indexOf(' ', index + 1);
    i++;
  } 
  if (index == 255) {
    return 0;
  }
  return index;
}

/**
 * Return the parameter of a command
 * Sample :   
 * String command = "M100 Z -10 A";
 * String result = getParameter(command, 1);
 * return Z
 * If no result, return false
 */
String getParameter (String data, byte rank) {
  byte start;
  byte end;
  //Start
  if (rank == 0) {
    start = 0;
  } else {
    start = indexOfRank(data, rank);
    if (start == 0) {
      //No paramater at this rank
      return String(false);
    }
    start ++; // remove space
  }
  
  //End
  end = indexOfRank(data, rank+1);
  if (end == 0) {
    //End of string
    end = data.length();
  }
  String result = data.substring(start, end);
  result.trim();
  return result;
}

// Say you are here !
void alive() {
  //String message;
  //message.concat(id);
  //message.concat(" ALIVE");
  //sendMessage(message); 
  sendMessage(String(String(id) + " ALIVE"));   
}


