/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */


#include <EEPROMex.h>

#define addressID 5 // Address where the id of the car is stored
#define addressMode 10 // Read the mode to run : 0 => Normal, 1 => Simple
#define addressSpeedSimple 15 // Read the mode to run : 0 => Normal, 1 => Simple
#define addressLevelSpeeds 16


// read the Id of the car
int eepromReadId () {
  return EEPROM.readInt(addressID);
}

// write the Id of the car
void eepromWriteId (String command) {
  if (getParameter(command, 2)) {
    id = getParameter(command, 2).toInt();
    Serial.print("ID changed : ");
    Serial.println(id);
    EEPROM.writeInt(addressID, id);
  }
}

// Read the mode to run : 0 => Normal, 1 => Simple
byte eepromReadMode () {
  return EEPROM.readByte(addressMode);
}

// write the Car mode
void eepromWriteMode (String command) {
  if (getParameter(command, 2)) {
    mode = getParameter(command, 2).toInt();
    Serial.print("Mode changed : ");
    Serial.println(mode);
    EEPROM.writeByte(addressMode, mode);
  }
}

// Read the speed for simple mode
byte eepromReadSpeedSimple () {
  return EEPROM.readByte(addressSpeedSimple);
}

// write the speed for simple mode
// Change speed of the car for simple mode : 2 SPS 30
void eepromWriteSpeedSimple (String command) {
  if (getParameter(command, 2)) {
    speedSimple = getParameter(command, 2).toInt();
    Serial.print("Speed changed : ");
    Serial.println(speedSimple);
    EEPROM.writeByte(addressSpeedSimple, speedSimple);
    setCarSpeed(String(String(id) + " SP " + String(speedSimple)));
  }
}

// Read the values level for speed
// And store it in levelSpeeds array
void eepromReadLevelsSpeeds () {
  byte i;
  for (i=0; i<7; i++) {
    levelSpeeds[i] = EEPROM.readByte(addressLevelSpeeds + i);
    if (true) {
      Serial.print("Level speed : ");      
      Serial.print(i);
      Serial.print("/");
      Serial.println(levelSpeeds[i]);
    }
  }  
}


// Write the values level for speed
// Change the 7 levels speed : 
// Easy : 2 WLS 0 23 28 33 38 43 48
// Medium : 0 WLS 0 34 39 44 49 54 59
// Hard : 0 WLS 0 39 52 65 78 91 99
void eepromWriteLevelsSpeeds (String command) {
  byte parameter = 2; 
  byte speedLevel;
  Serial.println("Levels speeds changed");  
  while (getParameter(command, parameter)) {
    speedLevel = getParameter(command, parameter).toInt();
    EEPROM.writeByte(addressLevelSpeeds + parameter - 2, speedLevel);
    parameter++;
  }
  eepromReadLevelsSpeeds();
}

