
bool blinkLeft = false;
bool blinkRight = false;
bool blinkStatus = false;


/*
  Light command : 2 LI F O (Light Front ON) 
  Second argument : B(back), L(left), R(right), F(front)
  Third argument : O(on), F(off), BO(blink on), BF(blink off), HF(HeadLight Flashing)
*/

void lights (String command) {
  String led = getParameter(command, 2);
  byte ledPin;
  if (led == "B") ledPin = PIN_LIGHT_BACK;
  if (led == "L") ledPin = PIN_LIGHT_LEFT;
  if (led == "R") ledPin = PIN_LIGHT_RIGHT;
  if (led == "F") ledPin = PIN_LIGHT_FRONT;  

  String mode = getParameter(command, 3);
  if (mode == "O") digitalWrite(ledPin, HIGH);
  if (mode == "F") digitalWrite(ledPin, LOW);
  if (mode == "BO") {
    
    if (led == "L") blinkLeft = true;
    if (led == "R") blinkRight = true;
    if (timer.isEnabled(timerBlink))  {
      timer.restartTimer(timerBlink);
    } else {
      timerBlink = timer.setInterval(BLINK_INTERVAL, lightsBlink);      
    }

  }
  if (mode == "BF") {
    digitalWrite(ledPin, LOW);
    if (led == "L") blinkLeft = false;
    if (led == "R") blinkRight = false;
    if (!blinkLeft && !blinkRight) timer.disable(timerBlink);
  }  
  if (mode == "HF") {
    timerHF = timer.setTimer(HF_INTERVAL, lightsHF, HF_NUMLBER*2 - 1);
    lightsHF();
  }  
}

void lightsBlink() {
  blinkStatus = !blinkStatus;
  if (blinkLeft) digitalWrite(PIN_LIGHT_LEFT, blinkStatus);
  if (blinkRight) digitalWrite(PIN_LIGHT_RIGHT, blinkStatus);
}

void lightsHF() {
  digitalWrite(PIN_LIGHT_FRONT, !digitalRead(PIN_LIGHT_FRONT));
}

void lightsStop() {
  digitalWrite(PIN_LIGHT_BACK, LOW);
}
