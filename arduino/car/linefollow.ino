/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */


int treshold = 100; // <Treshold = White
String cmdTC = String(false); // Command for next Cross
byte state = 0; // State of the TRCT5000 sensors
byte lastState = 0; // Last known state
byte sensors = B000; 

#define MANUAL 0
#define AUTO 1
/**
 * Driving mode, AUTO is following sensors, MANUAL is for testing purpose
 */
byte driveMode = AUTO;


/**
 * Difference speed for LEFT motor and RIGHT motor
 * -100 : Left = carSpeed, Right = 0
 * 100 : Left = 0, Right = carSpeed
 * 0 : Left = carSpeed, Right = carSpeed
 * -50 : Left = carSpeed, Right = carSpeed/2
 */
int diffSpeed = 0; 

#define STRAIGHT 0
#define TURN_LEFT 1
#define TURN_RIGHT 2
#define LINE_LEFT 3
#define LINE_RIGHT 4


byte correctStateLevel = CORRECT_LEVEL0;

byte turnState = STRAIGHT;// status : 0 Straight, 1 Left, 2 Right

#define CHANGE_LINE_OFF 0 // 0 => Off
#define CHANGE_LINE_START_000 // 1 => Change line but have 000 at start
#define CHANGE_LINE_LINE1 2 // 2 => Change Line, car is at the first line
#define CHANGE_LINE_BETWEEN 3 // 3 => Car is between the two lines
#define CHANGE_LINE_LINE2 4 // 4 => Car have reache the second line, finish.

byte changeLineState = CHANGE_LINE_OFF;  

//int An4, An5, An6; Debug TCRT 5000

/* Debug options */
byte debug_frequency = 120; // Frequency when the debug message is sending
byte debug_level = 0; // Counter for frequency

byte carSpeed = 0;
byte leftSpeed = 0;
byte rightSpeed = 0;
byte maxSpeed = 255;

void getState () {
  debugTurnStatus();
  byte oldState = state;  
  if (driveMode == MANUAL) return; // In manual mode when configuring the car. TODO : How to leave this mode ? reboot car for now
  sensors = B000;

  // Read sensors
/*  An6 = analogRead(A6);
  An5 = analogRead(A5);
  An4 = analogRead(A4);  
  if (An6 > treshold)  sensors = sensors | B001;
  if (An5 > treshold)  sensors = sensors | B010;
  if (An4 > treshold)  sensors = sensors | B100; //Debug TCRT 5000 */

  if (analogRead(A6) > treshold)  sensors = sensors | B001;
  if (analogRead(A5) > treshold)  sensors = sensors | B010;
  if (analogRead(A4) > treshold)  sensors = sensors | B100; 
  
  if (lastState <= 2 && sensors == B000) state = 0;  
  if (sensors == B001) state = 1;
  if (sensors == B011) state = 2;
  if (sensors == B010) state = 3;
  if (sensors == B111) state = 3;
  if (lastState == 3 && (sensors == B000 || sensors == B111 || sensors == B111) && turnState == TURN_RIGHT) state = 0; 
  if (lastState == 3 && (sensors == B000 || sensors == B111 || sensors == B111) && turnState == TURN_LEFT) state = 6; 
  if (sensors == B110) state = 4;
  if (sensors == B100) state = 5;
  if (lastState >= 4 && sensors == B000) state = 6;
  if (sensors == B101) state = 2;
  
  // State have changed, update lastState
  if (oldState != state) lastState = state;
  correctState();

}

/**
Correct the state
*/

void correctState() {
  if (turnState == STRAIGHT || (turnState == LINE_LEFT && changeLineState == 1) || (turnState == LINE_RIGHT && changeLineState == 1)) {
    // The state should be 3
   if (state == 0) diffSpeed = -CORRECT_LEVEL3;           
   if (state == 1) diffSpeed = -CORRECT_LEVEL2;       
   if (state == 2) diffSpeed = -CORRECT_LEVEL1;       
   if (state == 3) diffSpeed = CORRECT_LEVEL0;
   if (state == 4) diffSpeed = CORRECT_LEVEL1;
   if (state == 5) diffSpeed = CORRECT_LEVEL2;
   if (state == 6) diffSpeed = CORRECT_LEVEL3;
   if (turnState == LINE_LEFT || turnState == LINE_RIGHT) {
     if (state != 0 && state != 6) {
       changeLineState = CHANGE_LINE_LINE1;
     }
   }   
  }
  if (turnState == TURN_RIGHT) {
    // The state should be 5
   if (state == 0) diffSpeed = -CORRECT_LEVEL3;           
   if (state == 1) diffSpeed = -CORRECT_LEVEL3;       
   if (state == 2) diffSpeed = -CORRECT_LEVEL3;       
   if (state == 3) diffSpeed = -CORRECT_LEVEL2;
   if (state == 4) diffSpeed = -CORRECT_LEVEL1;
   if (state == 5) diffSpeed = CORRECT_LEVEL0;
   if (state == 6) diffSpeed = CORRECT_LEVEL3;          
  }  
  if (turnState == TURN_LEFT) {
    // The state should be 1
   if (state == 0) diffSpeed = -CORRECT_LEVEL3;           
   if (state == 1) diffSpeed = CORRECT_LEVEL0;       
   if (state == 2) diffSpeed = CORRECT_LEVEL1;       
   if (state == 3) diffSpeed = CORRECT_LEVEL2;
   if (state == 4) diffSpeed = CORRECT_LEVEL3;
   if (state == 5) diffSpeed = CORRECT_LEVEL3;
   if (state == 6) diffSpeed = CORRECT_LEVEL3;          
  }  
  if (turnState == LINE_RIGHT) {
    if (changeLineState == CHANGE_LINE_LINE1) {
      if (state == 6 || state ==0) { // Add state 6 ?
        diffSpeed = -CORRECT_LEVEL1;
        changeLineState = CHANGE_LINE_BETWEEN;
      } else {
        diffSpeed = -CORRECT_LEVEL1;
      }
    }
    
    if (changeLineState == CHANGE_LINE_BETWEEN) {
      if (state == 6 || state ==0) { // Add state 6 ?
        diffSpeed = -CORRECT_LEVEL1;
      } else {
        diffSpeed = -CORRECT_LEVEL1;
        turnState = STRAIGHT;        
      }      
    }
  } // End LINE_RIGHT

  if (turnState == LINE_LEFT) {
    if (changeLineState == CHANGE_LINE_LINE1) {
      if (state == 6 || state ==0) { // Add state 0 ?
        diffSpeed = CORRECT_LEVEL1;
        changeLineState = CHANGE_LINE_BETWEEN;
      } else {
        diffSpeed = CORRECT_LEVEL1;
      }
    }
    
    if (changeLineState == CHANGE_LINE_BETWEEN) {
      if (state == 6 || state ==0) { // Add state 0 ?
        diffSpeed = CORRECT_LEVEL1;
      } else {
        diffSpeed = CORRECT_LEVEL1;
        turnState = STRAIGHT;      
        changeLineState = 0;
      }      
    }
  } // End LINE_RIGHT
  
}

/**
TURN is a command to turn on a crossroad
Example
1 TURN L
Car with ID 1 turn LEFT
*/

void turn(String command) {
  String param = getParameter(command, 2);
  //param.trim();
  if (param == "L") turnState = TURN_LEFT;
  if (param == "R") turnState = TURN_RIGHT;  
  if (param == "S") turnState = STRAIGHT;    
}

/**
CL is a command to change LINE
Example
1 CL L
Car with ID 1 search a left line
*/

void changeLine(String command) {
  String param = getParameter(command, 2);
  if (param == "L" && turnState == LINE_LEFT) return;
  if (param == "R" && turnState == LINE_RIGHT) return;
  if (param == "L") turnState = LINE_LEFT;
  if (param == "R") turnState = LINE_RIGHT;
  if (state == 0 || state == 6) {
    changeLineState = 1; //changeLineState = CHANGE_LINE_START_000;
  } else {
    changeLineState = CHANGE_LINE_LINE1;
  }
} 



/**
 * Calculate left and right speed 
 **/

void setDCSpeed() {
  int tmpSpeed; // Use to calculate a speed > 255
  byte rear = false;
  if (diffSpeed == 0) {
    setMotorSpeed(motorLPin, motorLDirectionPin, carSpeed, motorLDirection);    
    setMotorSpeed(motorRPin, motorRDirectionPin, carSpeed, motorRDirection);      
  }
  
  // Turn RIGHT
  if (diffSpeed > 0 ) {

    if (diffSpeed > 100) {
      rear = true;
      diffSpeed = diffSpeed - 100;
    }
    rightSpeed = (int) -(diffSpeed - 100) * carSpeed * 0.01;
    
    tmpSpeed = (int) (diffSpeed + 100) * carSpeed * 0.01; 
    if (tmpSpeed > maxSpeed) {
      tmpSpeed = maxSpeed;
    }
    leftSpeed = tmpSpeed;
    setMotorSpeed(motorLPin, motorLDirectionPin, carSpeed, motorLDirection);
    if (rear) {
      setMotorSpeed(motorRPin, motorRDirectionPin, rightSpeed, !motorRDirection);
    } else {    
      setMotorSpeed(motorRPin, motorRDirectionPin, rightSpeed, motorRDirection);  
    }
  }
  // Turn LEFT
  if (diffSpeed < 0 ) {
    if (diffSpeed < -100) {
      rear = true;
      diffSpeed = diffSpeed + 100;
    }    
    leftSpeed = (int) -(-diffSpeed - 100) * carSpeed * 0.01;
    
    tmpSpeed = (int) (-diffSpeed + 100) * carSpeed * 0.01; 
    if (tmpSpeed > maxSpeed) {
      tmpSpeed = maxSpeed;
    }
    rightSpeed = tmpSpeed;  
    if (rear) {
      setMotorSpeed(motorLPin, motorLDirectionPin, leftSpeed, !motorLDirection); 
    } else {
      setMotorSpeed(motorLPin, motorLDirectionPin, leftSpeed, motorLDirection); 
    }
    setMotorSpeed(motorRPin, motorRDirectionPin, carSpeed, motorRDirection);     
  }
}

void setMotorSpeed(byte pinSpeed, byte pinDirection, byte motorSpeed, byte motorDirection ) {
  if (motorSpeed == 0) {
    digitalWrite(pinDirection, LOW);
    digitalWrite(pinSpeed, LOW);    
  }
  if (motorDirection) {
    digitalWrite(pinDirection, HIGH);      
  } else {
    digitalWrite(pinDirection, LOW);      
  }

  if (motorDirection == HIGH) {
    analogWrite(pinSpeed,255 -motorSpeed);
  } else {
    analogWrite(pinSpeed,motorSpeed);
  }
  
}

/**
STEP is a command to configure the motor speed
Example
1 SP 90
Car with ID 1 goes to speed 90% of maxSpeed
1 SP 0
Car with ID 1 goes stop
1 SP 90 M
Car with ID 1 goes to speed 90% of maxSpeed in MANUAL mode
1 SP -50 D
Car with ID 1 have diffSpeed of 50% to the right
*/

void setCarSpeed(String command) {

//  carSpeed = (int) getParameter(command, 2).toInt() * maxSpeed / 100;
  /* Manual command */
  
  if ((getParameter(command, 3)) == "")  {
    driveMode = AUTO;    
    carSpeed = (int) getParameter(command, 2).toInt() * maxSpeed * 0.01;      
  }
    else  
  {
    driveMode = MANUAL;
    if (getParameter(command, 3) == "M") {
      carSpeed = (int) getParameter(command, 2).toInt() * maxSpeed * 0.01;
      
    }
    if (getParameter(command, 3) == "D") {
      diffSpeed = getParameter(command, 2).toInt();
    }    
  }
}  

void setCarSpeedLevel(String command) {
  byte newSpeed = getParameter(command, 2).toInt();
  if (LIGHTS && (newSpeed < levelSpeed)) {
       /* Serial.print("newSpeed : ");
        Serial.println(newSpeed);
        Serial.print("levelSpeed : ");
        Serial.println(levelSpeed); */
    digitalWrite(PIN_LIGHT_BACK, HIGH);
    timer.disable(timerStop);
    timerStop = timer.setTimeout(2000, lightsStop);
  }  
  if (LIGHTS && (newSpeed > levelSpeed)) {
    timer.disable(timerStop);
    lightsStop();
  }  
  
  byte mySpeed = levelSpeeds[newSpeed];
  if (mySpeed >=0 && mySpeed <=100) {
    carSpeed = mySpeed * maxSpeed * 0.01;      
    levelSpeed = newSpeed;
  }
  
}  

/*
Set the direction for the next cross.
1 TC 2 R => Turn right at cross 2
*/
void turnCross(String command) {
  cmdTC = command;
}

/*
Set the direction for the next cross.
1 TC 2 R => Turn right at cross 2
*/
void testMotors(String command) {
  // Left motor forward
/*  digitalWrite(motorLDirectionPin, LOW);
  analogWrite(motorLPin,60);  
  delay(3000);
  digitalWrite(motorLDirectionPin, LOW);
  digitalWrite(motorLPin,LOW);  */
  
}



void debugTurnStatus() {
  if (true) return;
  debug_level++;
  if (debug_level == debug_frequency) {
    debug_level = 0;  
    String debug;
    if (true) {
      // Change Line Debug
      if (true) {
        debug.concat("TS");
        debug.concat(turnState);    
        debug.concat("S");
        debug.concat(state);        
        debug.concat("CLS");
        debug.concat(changeLineState);        
        debug.concat("SE");
        debug += String(sensors, BIN);       
      }
      
      if (false) { 
        debug.concat(analogRead(A6));
        debug.concat(",");    
        debug.concat(analogRead(A5));
        debug.concat(",");    
        debug.concat(analogRead(A4));// debug TCRT5000  */
      }
      if (false) {
        debug.concat("CS");
        debug.concat(carSpeed);        
        debug.concat("DS");
        debug.concat(diffSpeed);        
        debug.concat("LS");
        debug.concat(leftSpeed);        
        debug.concat("RS");
        debug.concat(rightSpeed);                
      }
      if (false) {
        if (turnState == TURN_LEFT) debug.concat("L");
        if (turnState == TURN_RIGHT) debug.concat("R");
        if (turnState == STRAIGHT) debug.concat("S");         
        debug.concat("|");    
        debug += String(sensors, BIN);
        debug.concat("S:");     
        debug.concat(state);
        debug.concat("CS:"); 
        debug.concat(carSpeed);
        debug.concat("D:");  
        debug.concat(diffSpeed);
      }
      if (false) {      
        debug.concat("S:");     
        debug.concat(state);
      }      
      //debug.concat(speedR);  
      sendMessage(debug);
    }
    Serial.println(debug);
  }
}
