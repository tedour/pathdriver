/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#define DEBUG_RFID false
#define rfidBlock 2 //Block for RFID

String cmdRfidUid = String(false);

// read rfid block
void scanRFID() {
  
  //RFID
  // Look for new cards
  if ( mfrc522.PICC_IsNewCardPresent()) {
    // Select one of the cards
    if (  mfrc522.PICC_ReadCardSerial()) {
      if (cmdRfidUid.length() > 1) {
        if (DEBUG_RFID) {
          Serial.print("cmdRfidUid len : ");
          Serial.println(cmdRfidUid.length());          
        }
        writeRFIDUID(cmdRfidUid);
      }
      //Read the block
      byte readbackblock[18];//This array is used for reading out a block. The MIFARE_Read method requires a buffer that is at least 18 bytes to hold the 16 bytes of a block.
      readBlock(rfidBlock, readbackblock);//read the block back
      
      int uid = readbackblock[0] * 256 + readbackblock[1];
      byte rfidOptions = readbackblock[2]; 
      byte rfidSpeed = readbackblock[3];       
      byte rfidLights = readbackblock[4];        
       
      // Show some details of the PICC (that is: the tag/card)
      //String message;
      //message = String(id) + " POS " + uid;
      //sendMessage(message); 
      sendMessage(String(id) + " POS " + uid);
      if (DEBUG_RFID) {
        /*
          // Read the whole block
        for (byte i=0; i<18; i++) {
          Serial.print("readbackblock : ");
          Serial.println(readbackblock[i]);                  
        }
        */
        /*
        for (byte i=0; i<8; i++) {
          Serial.print("lights : ");
          Serial.print(bitRead(rfidLights, i));                            
          Serial.print(" / i : ");
          Serial.println(i);                            
        }
        */
        Serial.print("UID / options : ");
        Serial.println(uid);
        Serial.print("options : ");
        Serial.println(rfidOptions);
        Serial.print("speed : ");
        Serial.println(rfidSpeed);
        Serial.print("lights : ");
        Serial.println(rfidLights);       
        
      }
      
      // Options
      // First (0) byte is a Place 
      // Second (1) byte is a Cross road
      // Third (2) byte us a Cross road end
      // Fourth (3) byte is turn RIGHT
      // Fifth (4) byte is turn LEFT
      if (!bitRead(rfidOptions, 1)) { 
        //String command = String(id) + " TURN S";
        turn(String(id) + " TURN S");
      } else {
        // Cross
        if (cmdTC) {
          int rid = (int) getParameter(cmdTC, 2).toInt();
          if (rid == uid) {
            // default direction
            //Serial.print("Crossroad command :");
            //Serial.println(cmdTC);            
            //String command = String(id) + " TURN " + getParameter(cmdTC, 3);          
            //turn(command);
            
            //String turnDirection = getParameter(cmdTC, 3);
            //Serial.print("turnDirection :");
            //Serial.println(turnDirection); 
            turn(String(id) + " TURN " + getParameter(cmdTC, 3));
            if (LIGHTS && rfidLights > 0) {
              lights(String(id) + " LI " + getParameter(cmdTC, 3) + " BO");
            }            
            cmdTC = String(false);
          }
        } else {
          
          //String command = String(id) + " TURN S";
          //turn(command);
          if (bitRead(rfidOptions,3)) {
            // Turn right
            turn(String(id) + " TURN R");
            if (LIGHTS && rfidLights > 0 && bitRead(rfidLights, 2)) {
              lights(String(id) + " LI L BO");
            }
          } else if (bitRead(rfidOptions,4)) {
            // Turn left
            turn(String(id) + " TURN L");
            if (LIGHTS && rfidLights > 0 && bitRead(rfidLights, 3)) {
              lights(String(id) + " LI R BO");
            }            
          } else {
            turn(String(id) + " TURN S");
          }
          
          } 

      }; 
      
      // Speed
      if (rfidSpeed > 0 && rfidSpeed < levelSpeed) {
        //setCarSpeedLevel(String(id) + " SPL " + rfidSpeed); //2 SPL 2
      }
      
      // Lights
      
      if (LIGHTS && rfidLights > 0) {
        
        
        if (bitRead(rfidLights, 0)) {
          //Turn on lights
          lights(String(id) + " LI F O");
          lights(String(id) + " LI B O");          
        }
        if (bitRead(rfidLights, 1)) {          
          //Turn off lights
          lights(String(id) + " LI F F");          
          lights(String(id) + " LI B F");                    
        }
        if (bitRead(rfidLights, 4)) {
          //End turn left or right
          lights(String(id) + " LI L BF");          
          lights(String(id) + " LI R BF");           
        }        
        
        
      }
      
      // Halt PICC
      mfrc522.PICC_HaltA();
      // Stop encryption on PCD
      mfrc522.PCD_StopCrypto1();        
    }
  }    
}

// Store uid to write it when a RFID is scanned
// 2 RID 14 5 3 1 => Change the next RFID UID that will be scan
// 14 => rfid UID
// 5 Direction =>       // First (0) byte is a Place 
      // Second (1) byte is a Cross road
      // Third (2) byte us a Cross road end
      // Fourth (3) byte is turn RIGHT
      // Fifth (4) byte is turn LEFT
// 3 Speed => // 0 => Don't change speed
      // 1 => Set speed to 1
      // 5 => Set speed to 5
// 1 lights => // First (0) byte light on
      // Second (1) byte light off
      // Third (2) turn signal on (left or right)
      // Fourth (3) turn signal off (left and right)

void RFIDprepareUID(String command) {
  cmdRfidUid = command;
}

// Change rfid uid tag

void writeRFIDUID(String cmd) {
  int uidCmd = (int) getParameter(cmd, 2).toInt();
  if (DEBUG_RFID) {
        Serial.print("options : ");
        Serial.println((byte) getParameter(cmd, 3).toInt());
        Serial.print("speed : ");
        Serial.println((byte) getParameter(cmd, 4).toInt());
        Serial.print("lights : ");
        Serial.println((byte) getParameter(cmd, 5).toInt());        
  }
  // Uid is stored in two bytes
  byte lowByte = (uint8_t) ((uidCmd) & 0xff);
  byte highByte = (uint8_t) ((uidCmd) >> 8);  
    
  byte blockContent[5] = {highByte, lowByte, (byte) getParameter(cmd, 3).toInt(), (byte) getParameter(cmd, 4).toInt(), (byte) getParameter(cmd, 5).toInt()}; 
  writeBlock(rfidBlock, blockContent); 
  cmdRfidUid = String(false);
}


/*
 * Based on code Dr.Leong   ( WWW.B2CQSHOP.COM )
 * Created by Miguel Balboa (circuitito.com), Jan, 2012.
 * Rewritten by Søren Thing Andersen (access.thing.dk), fall of 2013 (Translation to English, refactored, comments, anti collision, cascade levels.)
 */

       
int writeBlock(int blockNumber, byte arrayAddress[]) 
{
  //this makes sure that we only write into data blocks. Every 4th block is a trailer block for the access/security info.
  int largestModulo4Number=blockNumber/4*4;
  int trailerBlock=largestModulo4Number+3;//determine trailer block for the sector
  if (blockNumber > 2 && (blockNumber+1)%4 == 0){Serial.print(blockNumber);Serial.println(" is a trailer block:");return 2;}//block number is a trailer block (modulo 4); quit and send error code 2
  if (DEBUG_RFID) {
    Serial.print(blockNumber);
    Serial.println(" is a data block:");
  }
  
  /*****************************************authentication of the desired block for access***********************************************************/
  byte status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  //byte PCD_Authenticate(byte command, byte blockAddr, MIFARE_Key *key, Uid *uid);
  //this method is used to authenticate a certain block for writing or reading
  //command: See enumerations above -> PICC_CMD_MF_AUTH_KEY_A	= 0x60 (=1100000),		// this command performs authentication with Key A
  //blockAddr is the number of the block from 0 to 15.
  //MIFARE_Key *key is a pointer to the MIFARE_Key struct defined above, this struct needs to be defined for each block. New cards have all A/B= FF FF FF FF FF FF
  //Uid *uid is a pointer to the UID struct that contains the user ID of the card.
  if (status != MFRC522::STATUS_OK) {
         if (DEBUG_RFID) {
           Serial.print("PCD_Authenticate() failed: ");
           Serial.println(mfrc522.GetStatusCodeName(status));
         }
         return 3;//return "3" as error message
  }
  //it appears the authentication needs to be made before every block read/write within a specific sector.
  //If a different sector is being authenticated access to the previous one is lost.


  /*****************************************writing the block***********************************************************/
        
  status = mfrc522.MIFARE_Write(blockNumber, arrayAddress, 16);//valueBlockA is the block number, MIFARE_Write(block number (0-15), byte array containing 16 values, number of bytes in block (=16))
  //status = mfrc522.MIFARE_Write(9, value1Block, 16);
  if (status != MFRC522::STATUS_OK) {
           if (DEBUG_RFID) {
            Serial.print("MIFARE_Write() failed: ");
           Serial.println(mfrc522.GetStatusCodeName(status));
           }
           return 4;//return "4" as error message
  }
  Serial.println("block was written");
}


int readBlock(int blockNumber, byte arrayAddress[]) 
{
  int largestModulo4Number=blockNumber/4*4;
  int trailerBlock=largestModulo4Number+3;//determine trailer block for the sector

  /*****************************************authentication of the desired block for access***********************************************************/
  byte status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  //byte PCD_Authenticate(byte command, byte blockAddr, MIFARE_Key *key, Uid *uid);
  //this method is used to authenticate a certain block for writing or reading
  //command: See enumerations above -> PICC_CMD_MF_AUTH_KEY_A	= 0x60 (=1100000),		// this command performs authentication with Key A
  //blockAddr is the number of the block from 0 to 15.
  //MIFARE_Key *key is a pointer to the MIFARE_Key struct defined above, this struct needs to be defined for each block. New cards have all A/B= FF FF FF FF FF FF
  //Uid *uid is a pointer to the UID struct that contains the user ID of the card.
  if (status != MFRC522::STATUS_OK) {
    if (DEBUG_RFID) {
         Serial.print("PCD_Authenticate() failed (read): ");
         Serial.println(mfrc522.GetStatusCodeName(status));
    }
         return 3;//return "3" as error message
  }
  //it appears the authentication needs to be made before every block read/write within a specific sector.
  //If a different sector is being authenticated access to the previous one is lost.


  /*****************************************reading a block***********************************************************/
        
  byte buffersize = 18;//we need to define a variable with the read buffer size, since the MIFARE_Read method below needs a pointer to the variable that contains the size... 
  status = mfrc522.MIFARE_Read(blockNumber, arrayAddress, &buffersize);//&buffersize is a pointer to the buffersize variable; MIFARE_Read requires a pointer instead of just a number
  if (status != MFRC522::STATUS_OK) {
    if (DEBUG_RFID) {
          Serial.print("MIFARE_read() failed: ");
          Serial.println(mfrc522.GetStatusCodeName(status));
    }          
          return 4;//return "4" as error message
  }
  if (DEBUG_RFID) Serial.println("block was read");
}
