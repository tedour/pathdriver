/*
  HG7881_Motor_Driver_Example - Arduino sketch
   
  This example shows how to drive a motor with using HG7881 (L9110) Dual
  Channel Motor Driver Module.  For simplicity, this example shows how to
  drive a single motor.  Both channels work the same way.
   
  This example is meant to illustrate how to operate the motor driver
  and is not intended to be elegant, efficient or useful.
   
  
  Related Banana Robotics items:
   
    BR010038 HG7881 (L9110) Dual Channel Motor Driver Module
    https://www.BananaRobotics.com/shop/HG7881-(L9110)-Dual-Channel-Motor-Driver-Module
 
  https://www.BananaRobotics.com
*/
 
// Define mode :
// mode 1 : the two motors will turn in each direction at two different speeds : 
// Left slow - Left fast - Left slow reverse - Left fast reverse - Right slow - Right fast - Right slow reverse - Right fast reverse
// mode 2 : The car will progress Straight at PWN_SPEED
#define MODE 1
 
 
// functional connections
#define MOTOR_A_PWM 3 // Motor A PWM Speed
#define MOTOR_A_DIR 2 // Motor A Direction

// functional connections
#define MOTOR_B_PWM 5 // Motor B PWM Speed
#define MOTOR_B_DIR 4 // Motor B Direction

// the actual values for "fast" and "slow" depend on the motor
#define PWM_SPEED 100 // arbitrary fast speed PWM duty cycle
#define PWM_SLOW 80  // arbitrary slow speed PWM duty cycle
#define PWM_FAST 200 // arbitrary fast speed PWM duty cycle
#define DIR_DELAY 1000 // brief delay for abrupt motor changes
#define DIR_DELAY_WORK 3000 // brief delay for abrupt motor changes
 
void setup()
{
  Serial.begin( 9600 );
  pinMode( MOTOR_B_DIR, OUTPUT );
  pinMode( MOTOR_B_PWM, OUTPUT );
  digitalWrite( MOTOR_B_DIR, LOW );
  digitalWrite( MOTOR_B_PWM, LOW );
  pinMode( MOTOR_A_DIR, OUTPUT );
  pinMode( MOTOR_A_PWM, OUTPUT );
  digitalWrite( MOTOR_A_DIR, LOW );
  digitalWrite( MOTOR_A_PWM, LOW );  
}
 
void loop()
{ 
  if (MODE == 1) {
   
    
    Serial.println( "Left slow" );
    stop();
    // set the motor speed and direction
    digitalWrite( MOTOR_B_DIR, LOW ); // direction = reverse
    analogWrite( MOTOR_B_PWM, PWM_SLOW ); // PWM speed = slow
    delay( DIR_DELAY_WORK );  
  
    Serial.println( "Left fast" );
    stop();
    // set the motor speed and direction
    digitalWrite( MOTOR_B_DIR, LOW ); // direction = reverse
    analogWrite( MOTOR_B_PWM, PWM_FAST ); // PWM speed = slow
    delay( DIR_DELAY_WORK );    
    
    Serial.println( "Left slow reverse" );
    stop();
    // set the motor speed and direction
    digitalWrite( MOTOR_B_DIR, HIGH ); // direction = forward
    analogWrite( MOTOR_B_PWM, 255-PWM_SLOW ); // PWM speed = slow  
    delay( DIR_DELAY_WORK );
  
    Serial.println( "Left fast reverse" );
    stop();
    // set the motor speed and direction
    digitalWrite( MOTOR_B_DIR, HIGH ); // direction = forward
    analogWrite( MOTOR_B_PWM, 255-PWM_FAST ); // PWM speed = slow  
    delay( DIR_DELAY_WORK );    
    
    Serial.println( "Right slow" );
    stop();
    // set the motor speed and direction
    digitalWrite( MOTOR_A_DIR, HIGH ); 
    analogWrite( MOTOR_A_PWM, 255-PWM_SLOW ); 
    delay( DIR_DELAY_WORK );  
  
    Serial.println( "Right fast" );
    stop();
    // set the motor speed and direction
    digitalWrite( MOTOR_A_DIR, HIGH ); // direction = reverse
    analogWrite( MOTOR_A_PWM, 255-PWM_FAST ); // PWM speed = slow
    delay( DIR_DELAY_WORK );  
    
    Serial.println( "Right slow reverse" );
    stop();
    // set the motor speed and direction
    digitalWrite( MOTOR_A_DIR, LOW ); // direction = reverse
    analogWrite( MOTOR_A_PWM, PWM_SLOW ); // PWM speed = slow
    delay( DIR_DELAY_WORK );  
  
    Serial.println( "Right fast reverse" );
    stop();
    // set the motor speed and direction
    digitalWrite( MOTOR_A_DIR, LOW ); // direction = reverse
    analogWrite( MOTOR_A_PWM, PWM_FAST ); // PWM speed = slow
    delay( DIR_DELAY_WORK );     
  }
  if (MODE == 2) {

    Serial.println( "Start go straight" );
    //Left motor
    digitalWrite( MOTOR_A_DIR, HIGH ); // direction = reverse
    analogWrite( MOTOR_A_PWM, 255-PWM_SPEED ); // PWM speed = slow    
    //Right motor
    digitalWrite( MOTOR_B_DIR, LOW ); // direction = reverse
    analogWrite( MOTOR_B_PWM, PWM_SPEED ); // PWM speed = slow    
    delay(10000);
  }
  
}

void stop() {
  digitalWrite( MOTOR_B_DIR, LOW );
  digitalWrite( MOTOR_B_PWM, LOW );
  digitalWrite( MOTOR_A_DIR, LOW );
  digitalWrite( MOTOR_A_PWM, LOW );  
  delay( DIR_DELAY );  
}
/*EOF*/
