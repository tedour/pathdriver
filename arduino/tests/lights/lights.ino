/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
#define PIN_LIGHT_BACK 14 // A0
#define PIN_LIGHT_LEFT 15 // A1
#define PIN_LIGHT_RIGHT 16 // A2
#define PIN_LIGHT_FRONT 17 // A3

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(PIN_LIGHT_BACK, OUTPUT);     
  pinMode(PIN_LIGHT_LEFT, OUTPUT);     
  pinMode(PIN_LIGHT_RIGHT, OUTPUT);     
  pinMode(PIN_LIGHT_FRONT, OUTPUT);       
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(PIN_LIGHT_FRONT, LOW);   // turn the LED on (HIGH is the voltage level)    
  digitalWrite(PIN_LIGHT_BACK, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1500);               // wait for a second  
  digitalWrite(PIN_LIGHT_BACK, LOW);   // turn the LED on (HIGH is the voltage level)  
  digitalWrite(PIN_LIGHT_LEFT, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1500);               // wait for a second  
  digitalWrite(PIN_LIGHT_LEFT, LOW);   // turn the LED on (HIGH is the voltage level)  
  digitalWrite(PIN_LIGHT_RIGHT, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1500);               // wait for a second  
  digitalWrite(PIN_LIGHT_RIGHT, LOW);   // turn the LED on (HIGH is the voltage level)  
  digitalWrite(PIN_LIGHT_FRONT, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1500);               // wait for a second    
}
