/*
  TCRT 5000L
  Test if your TCRT 5000L line sensors arez working
 */
 

// the setup routine runs once when you press reset:
void setup() {  
  Serial.begin(9600);  
}

// the loop routine runs over and over again forever:
void loop() {
  String debug;
  debug.concat("Left/Middle/Right: ");    
  debug.concat(analogRead(A4));
  debug.concat("|");    
  debug.concat(analogRead(A5));
  debug.concat("|");    
  debug.concat(analogRead(A6));// debug TCRT5000
  Serial.println(debug);  
  delay(500);               // wait for a second
}
