/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

Template.carEdit.helpers({
  car: function () {
    return Cars.findOne({cid: String(this.cid)});
  }
});

Template.carEdit.events ({
  "change .speed": _.debounce(function (event) {
    var speed = event.target.value;
    // Send speed to car
    message = this.cid + " SP " + speed + " M";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    return true;
  },300, true),
  "submit .car-name": function (event) {
    // Store element of form
    var name = event.target.name.value;
    var cid = event.target.cid.value;
    // alert("cid:" + cid + "/name : " + name);
    // Update Car
    Cars.update({_id:Cars.findOne({cid:cid})['_id']}, {$set:{name: name}});
    return true;
  },
  "submit .car-gearbox": function (event) {
    message = this.cid + " WLS 0 " + event.target.gear1.value + " " + event.target.gear2.value + " " + event.target.gear3.value + " " + event.target.gear4.value + " " + event.target.gear5.value + " " + event.target.gear6.value;
    console.log(message);
    
    // Meteor.call('toArduino', message,  function (error, result) {
    //   if (error) {
    //     // handle error
    //     // console.log(error);
    //   } else {
    //     // console.log(result);
    //   }
    // });
    return true;
  },
  "submit .car-id": function (event) {
    // Save element of form
    var name = event.target.name.value;
    var cid = event.target.cid.value;
    // alert("cid:" + cid + "/name : " + name);
    // Update Car
    Cars.update({_id:Cars.findOne({cid:cid})['_id']}, {$set:{name: name}});
    return true;
  }
});
