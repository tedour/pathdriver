/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

Template.carsList.helpers({
  activeCars : function() {
    if (Session.get("selectedCar")) {
      return Cars.find({'active':true, 'cid': Session.get("selectedCar")}, {});
    } else {
      return Cars.find({'active':true}, {});
    }
  },
  activeCarsByAmount : function() {
    return Cars.find({'active':true}, {sort:{amount:-1}});
  },
  getStep : function() {
    step = TrackStep.findOne({'idOnMap': this.idOnMap});
    return step;
  },
  trackConfig : function() {
    if (TrackStep.find({rid: 0}).count() > 0) {
      var result = TrackStep.findOne({rid: 0});
    } else {
      var result = [{
        name : "No track"
      }];
    }
    console.log(result);
    return result;
  },
  disabledCars : function() {
    if (Session.get("selectedCar")) {
      return false;
    } else {
      return Cars.find({'active':false}, {});
    }
  },
  getCountDown : function() {
    return config = Config.findOne({'key':'countDown'}, {fields:{'countDownValue':1}});
  },
  getPlacesOrder : function() {
    places = TrackStep.find({'place': true}, {sort:{placeValue:-1}});
    return places;
  },
  getPlacesFirst : function() {
    placesCount = TrackStep.find({'place': true}, {}).count();
    places = TrackStep.find({'place': true}, {'limit' : Math.floor(placesCount/2)});
    return places;
  },
  getPlacesLast : function() {
    placesCount = TrackStep.find({'place': true}, {}).count();
    places = TrackStep.find({'place': true}, {'skip' : Math.floor(placesCount/2),'limit' : Math.ceil(placesCount/2)});
    return places;
  },
  getCross : function() {
    if (this.hasOwnProperty('nextcross')) {
      return TrackStep.findOne({'rid': this.nextcross}, {});
    }
  },
  isGame1 : function () {
    return Config.findOne({key: "game", game: "game1"}); // TODO : Move to TrackStep 0 ?
  },
  isGameRace : function () {
    return Config.findOne({key: "game", game: "gameRace"}); // TODO : Move to TrackStep 0 ?
  },
  noGame : function () {
    return false;
  }
});

Template.carsList.events ({
  "click .reboot-car": function (event) {
    var car = Cars.findOne({'cid':this.cid}, {});
    message = this.cid + " RBT";
    Meteor.call('toArduino', message,  function (error, result) {});
    //Speed to zero
    Meteor.call('updateSpeed', this.cid, 0,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
  },
  "click .ia-car": function (event) {
    var car = Cars.findOne({'cid':this.cid}, {});
    if (car.hasOwnProperty('ia') && car.ia == false) {
      Meteor.call("setIA", this.cid, true);
      $(event.currentTarget).addClass('active');
    } else {
      Meteor.call("setIA", this.cid, false);
      $(event.currentTarget).removeClass('active');
    }
  },
  "click .select-car": function (event) {
    if (Session.get("selectedCar")) {
      Session.set("selectedCar", false);
      $(event.currentTarget).removeClass('active');
    } else {
      Session.set("selectedCar", this.cid);
      $(event.currentTarget).addClass('active');
    }
    return true;
  },
  "click .disconnect-car": function (event) {

    // All cars are disabled cars
    // console.log(this);
    Meteor.call("disableCar", this.cid);
    return true;
  },
  "click .countdown": function (event) {
    Meteor.call("startCountDown");
    return true;
  },
  "change .speed": function (event) {
    speed = event.target.value;
    // Send speed to car
    message = this.cid + " SPL " + speed;
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
  },
  "click .headlight-flash": function (event) {
    message = this.cid + " LI F HF";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    return true;
  },
  "click .light-on": function (event) {
    message = this.cid + " LI F O";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    message = this.cid + " LI B O";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    return true;
  },
  "click .light-off": function (event) {
    message = this.cid + " LI F F";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    message = this.cid + " LI B F";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    return true;
  },
  "click .warning": function (event) {
    message = this.cid + " LI L BO";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    message = this.cid + " LI R BO";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    return true;
  },
  "click .speed-minus": function (event) {
    console.log(this);
    var speed = this.speed;
    if (speed > 0) {
      speed--;
    }
    console.log(speed);
    Meteor.call('updateSpeed', this.cid, speed,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    message = this.cid + " SPL " + speed;
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    return true;
  },
  "click .speed-plus": function (event) {
    console.log(this);
    var speed = this.speed;
    // maxSpeed
    if (this.hasOwnProperty("maxSpeed") && this.maxSpeed > 0 && speed >= this.maxSpeed ) {
      return false;
    }

    if (speed < 6) {
      speed++;
    }
    Meteor.call('updateSpeed', this.cid, speed,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    message = this.cid + " SPL " + speed;
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      }
    });
    return true;
  },
  "click .left": function (event) {
    // Send turn left to car
    message = this.cid + " TURN L";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      };
    });
    return true;
  },
  "click .right": function (event) {
    // Send turn right to car
    message = this.cid + " TURN R";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      };
    });
    return true;
  },
  "click .straight": function (event) {
    // Send go straight to car
    message = this.cid + " TURN S";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      };
    });
    return true;
  },
  "click .left-cross": function (event, template) {
    // Send go left at next cross to car
    // Better way with parentData ?
    cid = $(event.currentTarget).data('cid');
    $(".next-step-picture button").removeClass('active');
    $(event.currentTarget).addClass('active');
    var message = cid + " TC " + this.rid + " L";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      };
    });
    return true;
  },
  "click .right-cross": function (event, template) {
    // Send go right at next cross to car
    // Better way with parentData ?
    cid = $(event.currentTarget).data('cid');
    $(".next-step-picture button").removeClass('active');
    $(event.currentTarget).addClass('active');
    console.log(cid);
    var message = cid + " TC " + this.rid + " R";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      };
    });
    return true;
  },
  "click .change-line-left": function (event, template) {
    // Send go left at next cross to car
    cid = $(event.currentTarget).data('cid');
    var car = Cars.findOne({'cid':this.cid}, {});
    if (car.changeLine == 0 || car.changeLine == 1) {
      return true;
    }
    var message = cid + " CL L";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      };
    });
    return true;
  },
  "click .change-line-right": function (event, template) {
    // Send go left at next cross to car
    // Better way with parentData ?
    cid = $(event.currentTarget).data('cid');
    var car = Cars.findOne({'cid':this.cid}, {});
    if (car.changeLine == 0 || car.changeLine == 2) {
      return true;
    }
    var message = cid + " CL R";
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      };
    });
    return true;
  },

});
