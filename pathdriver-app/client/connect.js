/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */


Template.connectArduino.helpers({
  serialPorts : function() {
    return Config.findOne({key: "serialPorts"});
  }
});

Template.connectArduino.events({
  "click .refresh-serial-ports": function () {
    //Refresh list now
    Meteor.call('serialPortsRefresh', function (error, result) {
      if (error) {
        // handle error
        console.log(error);
      } else {
        console.log(result);
      };
    });
  },
  "click .connect-serial-port": function () {
    //Refresh list now
    Meteor.call('serialPortConnect', Config.findOne({key: "serialPorts"}),  function (error, result) {
      if (error) {
        // handle error
        console.log(error);
      } else {
        console.log(result);
      };
    });
  },
  "click .ping": function () {
    //Refresh list now
    Meteor.call('ping', function (error, result) {
      if (error) {
        // handle error
        console.log(error);
      } else {
        console.log(result);
      };
    });
  }
});
