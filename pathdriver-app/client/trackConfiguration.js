/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */


Template.trackConfiguration.helpers({
  trackConfig : function() {
    if (TrackStep.find({rid: 0}).count() > 0) {
      var result = TrackStep.find({rid: 0});
    } else {
      var result = [{
        name : "No track"
      }];
    }
    return result;
  },
  trackSteps : function() {
    var result = TrackStep.find({rid: {$gt: 0} }, {sort: {rid: 1}});
    return result;
  }
});

Template.trackConfiguration.events ({
  "click .rfid-update": function (event) {
    // Send RFID update
    // Calculate options
    var options = 0;
    console.log(this);
    // Place, do nothing
    if (this.hasOwnProperty("place") && this.place) {
      options = options + 1;
    }
    // Cross, do nothing
    if (this.hasOwnProperty("cross") && this.cross) {
      options = options + 2;
      // Default direction in case off cross :
      // Straight (+0)
      // Go Right (+8)
      // Go Left (+16)
      if (this.hasOwnProperty("defaultDirection") && this.defaultDirection == "R") {
        options = options + 8;
      }
      if (this.hasOwnProperty("defaultDirection") && this.defaultDirection == "L") {
        options = options + 16;
      }
    }
    // Crossend, go straight
    if (this.hasOwnProperty("crossend") && this.crossend) {
      options = options + 4;
    }
    // Speed
    var speed = 0;

    // Lights
    var lights = 0;
    if (this.hasOwnProperty("lightOn") && this.lightOn) {
      lights = lights + 1;
    }
    if (this.hasOwnProperty("lightOff") && this.lightOff) {
      lights = lights + 2;
    }
    if (this.hasOwnProperty("turnSignalROn") && this.turnSignalROn) {
      lights = lights + 4;
    }
    if (this.hasOwnProperty("turnSignalLOn") && this.turnSignalLOn) {
      lights = lights + 8;
    }
    if (this.hasOwnProperty("turnSignalOff") && this.turnSignalOff) {
      lights = lights + 16;
    }




    message = 0 + " RID " + this.rid + " " + options + " " + speed + " " + lights; // O is speed, need to change
    console.log(message);
    Meteor.call('toArduino', message,  function (error, result) {
      if (error) {
        // handle error
        // console.log(error);
      } else {
        // console.log(result);
      };
    });
    return true;
  }
});
