/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

Template.trackExport.helpers({
  jsonTrackCode : function() {
    var results = TrackStep.find({});
    var jsonDatas = [];
    results.forEach(function (result) {
        delete result._id;// Remove mongodb ID
        jsonDatas.push(result);
    });
    var text = JSON.stringify(jsonDatas, {indent: true});
    return {json : text};
  }
});

Template.trackExport.events ({
  "click .upload": function (event) {
    //Get JSON code
    var textElements = $("textarea.track").val();
    var elements = JSON.parse(textElements);

    // Remove all elements from trackStep
    var result = Meteor.call("removeAllTrackStep");


    elements.forEach(function(element) {
      //Insert each trackStep
      console.log(element);
      TrackStep.insert(element);
    });
  }
});
