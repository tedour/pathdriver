Router.configure({
    layoutTemplate: 'layout'
});

Router.map(function() {
  //this.route('index', {path: '/'});
  this.route('connectArduino', {path: '/connect-arduino'});
  this.route('carsList', {path: '/'});
  this.route('trackConfiguration', {path: '/track-configuration'});
  this.route('log', {path: '/log'});
  this.route('confGame', {path: '/game-configuration'});
});
