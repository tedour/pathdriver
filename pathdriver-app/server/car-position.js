/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */


function carPosition (data) {
  // Update Car
  Cars.update({'cid':datas[0]}, {$set:{position: datas[2]}});

  //Look if we need to update RFID address for the track
  var myTrackElement = TrackStep.findOne({wait: true});
  if (myTrackElement) {
    console.log(myTrackElement);
    TrackStep.update({name: myTrackElement.name}, {$set:{rfidAddress: datas[2] }});
  }

}
