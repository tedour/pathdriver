/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

Meteor.startup(function () {
  SerialPort = Meteor.npmRequire('serialport');
  // Wrap method SerialPort.list to call it Synchronously
  listSerialPorts = function(callback) {
    SerialPort.list(function (err, ports) {

      callback(null, ports);
    });
  }
  // All cars are disabled cars
  Cars.update({}, {$set:{active: false, speed: 0}}, {multi: true});

});



Meteor.methods({
  refreshCarTracks: function () {
    console.log('refreshCarTracks');
  },
  serialPortsRefresh: function () {
    // TODO : problem when several arduinos ?
    Config.remove({key:'serialPorts'});
    // Call SerialPort.list
    var asyncListSerialPorts = Meteor.wrapAsync(listSerialPorts);
    //console.log(asyncListSerialPorts);
    var resultsListSerialPorts = asyncListSerialPorts();
    //console.log(resultsListSerialPorts);
    // Insert results in database
    var configSerialPorts = {key: "serialPorts", value: resultsListSerialPorts[0].comName };
    Config.insert(configSerialPorts);
  },
  // Connect Serial port
  serialPortConnect: function (port) {
    // debugger;
    // serialPort = new SerialPort(port.value, {baudrate: 9600});
    console.log('try port ' + port.value);

    serialPort = new SerialPort.SerialPort(port.value, {baudrate: 9600, parser: SerialPort.parsers.readline("\n")});
    // connectSerialPort(port);
    serialPort.on('open', function() {
        console.log('Port ' + port.value + ' open');
        lastConnect = new Date().valueOf();
    });
    serialPort.on('data', Meteor.bindEnvironment(function(data) {
        dispatchMessages(data);
    }, function(e) {
        throw e;
    }));
    sendToArduino = function(message) {
      console.log(message);
      serialPort.write(message + "\n");
    };
    dispatchMessages = function(data) {

      data = data.replace(/(\r\n|\n|\r)/gm,"");
      console.log(data);
      Meteor.call("watchdogDT", data);
      //Split data
      var datas = data.split(" ");
      // TODO : Test if the car is active

      // New Car
      if (datas[1] == "ALIVE") {

        // Look if the car is already in database
        if (Cars.findOne({'cid':datas[0]}) !== undefined) {
          // Update Car
          Cars.update({'cid':datas[0]}, {$set:{active: true}});
        } else {
          var cid = parseInt(datas[0],10);
          if (cid > 0) {
            // Add car to database
            Cars.insert({
              cid: datas[0],
              active: true,
              amount: 0,
              name: 'New car',
              image: 'taxi_small.png',
              speed: 0,
              rfidAddress : 0
            });
          }
        }
      }

      // get new RFID position
      if (datas[1] == "POS" && datas[0]) {
        //TODO : call a function in another file car-position.js
        // Update Car
        var myRid = parseInt(datas[2]);
        // Change amount if in place
        var myStep = TrackStep.findOne({'rid': myRid});
        if (myStep === undefined) return;
        cid = datas[0];
        myCar = Cars.findOne({'cid' : cid});
        if (myStep.hasOwnProperty("place") && myStep.place) {
          // Add value to car
          Cars.update({'cid':cid}, {$inc : {amount : myStep.placeValue}});
          // Remove value to place
          TrackStep.update({'rid': myRid}, {$set:{'placeValue' : 1}});
        }
        // Update rfidAddress
        Cars.update({'cid':datas[0]}, {$set:{'rfidAddress': myRid}});

        // check maxSpeed
        if (myStep.hasOwnProperty("maxSpeed")) {
          if (myStep.maxSpeed == 0) {
              Cars.update({'cid':datas[0]}, {$set:{'maxSpeed': 0}});
          } else if (myCar.speed > myStep.maxSpeed) {
              // Update the maxSpeed of the car
              Cars.update({'cid':datas[0]}, {$set:{'speed': myStep.maxSpeed}});
              Cars.update({'cid':datas[0]}, {$set:{'maxSpeed': myStep.maxSpeed}});
              //Send to Car
              message = myCar.cid + " SPL " + myStep.maxSpeed;
              Meteor.call('toArduino', message,  function (error, result) {});
          }
        }

        // Look at changeLine entry
        if (myStep.hasOwnProperty("changeLine")) {
          // Update the change line to the car
          Cars.update({'cid':datas[0]}, {$set:{'changeLine': myStep.changeLine}});
        }

        // Count laps
        if (myStep.hasOwnProperty("lapPosition")) {
          var currentLaps = 0, currentLapsDec = 0, currentLapsFloor = 0;
          var laps = 0;
          if (myCar.hasOwnProperty("lapsNumber")) {
            currentLaps = myCar.lapsNumber; // 5.8
            currentLapsDec = (myCar.lapsNumber % 1).toFixed(2); // 0.80
            currentLapsFloor = Math.floor(myCar.lapsNumber); // 5
          }
          if (currentLapsDec > myStep.lapPosition ) {
            // Add one lap
            laps = currentLapsFloor + myStep.lapPosition + 1;
          } else {
            laps = currentLapsFloor + myStep.lapPosition;
          }

          // Update the change line to the car
          Cars.update({'cid':datas[0]}, {$set:{'lapsNumber': laps}});
          Laps.insert({
            'cid' : datas[0],
            'laps' : laps,
            'time' : (new Date().valueOf() - lastConnect) / 1000
          });

        }


        //Check roadsign STOP
        if (myStep.hasOwnProperty("roadsign") && myStep.roadsign == "STOP") {
          //Look if there is anyone here
          step = Cars.findOne({'active': true, 'rfidAddress': myStep.roadsignRid});
          console.log(step);
          if (step && !(typeof myStop[myStep.rid] != undefined)) {
            myStop[myStep.rid] = Meteor.setInterval(function(){
              step = Cars.findOne({'active': true, 'rfidAddress': myStep.roadsignRid});
              if (step) {
                message = cid + " SPL 0";
                Meteor.call('toArduino', message,  function (error, result) {});
              } else {
                message = cid + " SPL " + myCar.cid;
                Meteor.call('toArduino', message,  function (error, result) {});
                Meteor.clearInterval(myStop[myStep.rid]);
              }
            }, 1000);
          }
        }

        //Update nextCross
        Meteor.call("findNextCross", myRid, function(error, result){
          if(error){
            Cars.update({'cid':datas[0]}, {$set:{'rid': parseInt(myRid)}});
          }
          if(result){
            Cars.update({'cid':datas[0]}, {$set:{'rid': myRid, 'nextcross': result}});
            var car = Cars.findOne({'cid':datas[0]}, {});
            if (car.hasOwnProperty('ia') && car.ia == true) {
              // IA is on
              if (Math.floor((Math.random()*2)) == 1) {
                var message = datas[0] + " TC " + result + " L";
              } else {
                var message = datas[0] + " TC " + result + " R";
              }
              Meteor.call('toArduino', message,  function (error, result) {
                if (error) {
                  // handle error
                  // console.log(error);
                } else {
                  // console.log(result);
                };
              });
            }
             console.log("Next cross : " + result);
          }
        });
      }

    };
  },

  toArduino: function (data) {
    sendToArduino(data);
  }


});
