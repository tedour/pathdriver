/*Copyright (C) 2015 - 2016  ROUDET Cyprien

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

Meteor.methods({
  findNextCross: function (rid) {
    step = TrackStep.findOne({'rid': rid});
    while (!(step.hasOwnProperty("cross") && step.cross)) {
      step = TrackStep.findOne({'rid': step.next[0].rid});
    }
    return step.rid;
  },
  startCountDown: function () {
    if (typeof countdown !== 'undefined') {
      Meteor.clearInterval(countdown);
    }
    var config = Config.findOne({'key':'countDown'}, {fields:{'countDownLength':1}});
    Config.update({'key':'countDown'}, {$set : {countDownValue : config.countDownLength}})
    countdown = Meteor.setInterval(function(){
      if (Config.findOne({'key':'countDown'}, {fields:{'countDownValue':1}}).countDownValue > 0) {
        Config.update({'key':'countDown'}, {$inc : {countDownValue : -1}})
      } else {
        Meteor.clearInterval(countdown);
        // Stop increasing places values
        if (typeof countPlaceValue !== 'undefined') {
          Meteor.clearInterval(countPlaceValue);
        }
        TrackStep.update({'place': true}, {$set:{'placeValue' : 0}}, {multi: true});
      };
    }, 1000);
    // Start increasing places values
    if (typeof countPlaceValue !== 'undefined') {
      Meteor.clearInterval(countPlaceValue);
    }
    // All places to 2$
    TrackStep.update({'place': true}, {$set:{'placeValue' : 2}}, {multi: true});
    // All cars to 0$
    Cars.update({}, {$set:{'amount' : 0}}, {multi: true});
    countPlaceValue = Meteor.setInterval(function(){
      TrackStep.update({'place': true}, {$inc : {'placeValue' : 1}}, {multi: true});
    }, 5000);
  },
  watchdogDT : function(message) {
    if (message.length == 0) {
      return;
    }
    if (message.substring(0,11) == "Sending : R") {
      return;
    }
    Watchdog.insert({'key': "data", 'value': message, 'createdAt' : (new Date().valueOf() - lastConnect) / 1000 });
  },
  watchdogRemove : function() {
    Watchdog.remove({});
  },
  disableCar : function(cid) {
    Cars.update({'cid' : cid}, {$set:{active: false}});
  },
  removeAllTrackStep: function() {
    TrackStep.remove({});
  },
  updateSpeed : function(cid, speed) {
    Cars.update({'cid' : cid}, {$set:{'speed': speed}});
  },
  setIA : function (cid, IA) {
    Cars.update({'cid' : cid}, {$set: {'ia': IA}});
  }

});
